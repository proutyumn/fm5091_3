function [result]=map(v, t, fn)

v(t)=fn(v(t));

if t<length(v)
    result = map(v, t + 1, fn);
else
    result = v;
end