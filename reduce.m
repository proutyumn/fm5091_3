function [result]=reduce(v, rs, t)

    rs = rs + v(t);
    
    if t<length(v)
        result = reduce(v, rs, t + 1);
    else
        result = rs;
    end
    

end